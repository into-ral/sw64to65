# Shopware 6 intoCommerce standard giude

<hr>

##  Table of contents
### 1.: Clone standard-poject from GitLab
### 2.: Initial intoCommerce standard project setup
### 3.: Merge existing project into project-standard
### 4.: Export database from stage/live system
#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4.1: Mittwald 
#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4.2: Maxcluster 
#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4.3: root360
### 5.: Import project database
### 6.: start a project
### 7.: X-Debug configuration
### 8.: PHP Version configuration & switch
### 9.: Node Version configuration & switch
### 10.: Remote host & local database configuration in PHPStorm
#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 10.1: Create Docker Container Connection via SFTP
#### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 10.2.: Create Docker Database Connection
### 11.: Working with watchers
### 12.: Working with makefile
### 13.: Troubleshooting
### 14.: Standard Plugins & Wildcard instance configuration

<hr>

## 1. Clone Standard Project from GitLab
1. Create a customer folder where the shop repository should be located.
2. Clone the project-specific repository in the created folder with: `git clone git@gitlab.com:path/to/project .`.
3. To clone the standard-project into the project-specific repository, use: `git archive --remote git@gitlab.com:ic_sha/intocommerce-standard-sha.git --format tar main | tar xf -`
4. All copied files from the standard-project should be displayed in the CLI. In some case, you need to check, that all files are copied.
5. Now you can make an initial commit in project-specific repository.

<hr>

## 2. Initial intoCommerce standard project setup
1. Open the `docker-compose.yml` and change following setting:<br>
   -> `image` - check the version is the same as the shopware version in `composer.lock` file. You can search there for `shopware/core`.<br>
   -> `container_name` - select a project specific name.
1. to start the project navigate in the terminal to docker-compose.yml and execute: `docker-compose up -d`.
2. to enter the docker-container terminal use `docker exec -it {{container-name}} bash`.
3. Run `composer install`.
4. Run `bin/console system:setup`
   - use `1` for dev mode
   - use `no` for no blue green deployment
   - use `root / root` for database user / passwort
   - use `yes` for write in `/var/www/html/.env`
5. Delete the file `install.lock`   
6. Run `bin/console system:install`.
7. Run `bin/console theme:compile && bin/console cache:clear`.
8. Open `http://localhost` and enjoy.

<hr>

## 3. Merge existing project into project-standard

<hr>

## 4. Export database from stage/live system
### 4.1 Mittwald
1. Connect with the server and navigate to project root path.
2. `cat .env` should show you the entry of .env file. Inside this file, there is a DATABASE_URL which looks like `DATABASE_URL="mysql://root1:root2@localhost:3306/shopware"`. The following parts from DATABASE_URL are interesting for us:
   - `root1` = user
   - `root2` = password
   - `localhost` = host
   - `3306` = port
   - `shopware` = database name
3. The next command, we need to fill with the correct credentials. The credentials you can find in the DATABASE_URL which you have `cat` from .env file. The command copys the complete database with all data inside a `filename.sql` file.<br> `mysqldump -u'root1' -p -h'localhost' 'shopware' < filename.sql` <br> The `-p` option let the command know, that you have to enter a password in cli. So when you fire the command, you need to paste a password. The password is also located in .env fiel (`root2`).
4. After command ends, you can do a `pwd` and copy the output. This is the location where you file is located on the server.
5. Navigate in the terminal to your local project folder, which is mapped to the docker container. To download the database backup, you can use this command `scp server_user@server_ip:/your/copied/path/from/pwd/filename.sql ./`. This commands copy's the file into the actual located folder from terminal.


### 4.2 Maxcluster
1. Open the `https://app.maxcluster.de/dashboard` and login.
2. Select your project-server.
3. Navigate to `Sicherheit & Backups` and then click on `Backups`
4. Scroll down to `Datenbanksicherungen`
5. Here click on the `Settings Icon`
6. A modal should popup and ask you to export the database. The Checkbox has to be checked. Before you click the big green button to export, copy the path on top of the modal. This is the path, were the database backup is located on the server. Now click the big green `Export Button`.
7. Navigate in the terminal to your local project folder, which is mapped to the docker container. To download the database backup, you can use this command `scp server_user@server_ip:/your/copied/path/to/backup/backup_example.sql ./`. This commands copy's the file into the actual located folder from terminal.

### 4.3 Root360
@todo

<hr>

## 5. Import project database
1. The next step is start the docker container and connect with the console. In the docker console you need to replace the actual shopware database. Use `mysql -uroot -proot shopware < backup_example.sql` to do this. If you have not the `backup_example.sql` file in the mapped folder, you should check that the file ist correctly located at you local machine.
2. When the import is done, you will get an error on `http://localhost` because of saleschannel domains. For fixing this error, you need to open `http://localhost/adminder.php`. The login credentials are `Host: localhost – User: root – Password: root`.
3. Select the `shopware` database and search for `sales_channel_domain`.
4. Here are the domains of the environment you have export the database. The domains has to be changed to `http://localhots` and `https://localhost`.

<hr>

## 6. Start a project
1. To start the project navigate in the terminal to docker-compose.yml and execute: `docker-compose up -d`.
2. For entering the docker-container terminal use `docker exec -it {{container-name}} bash`.

<hr>

## 7. X-Debug configuration
In docker-compose.yml at `shopware -> services -> environment` the variable `XDEBUG_ENABLED` should be set to `1` This is already configurated in our standard and means that you can configurate X-Debug use it.
1. Activate X-Debug extension in PHPStorm and your browser extension.
2. Open `http://localhost` in browser of your choice. PHPStorm should popup with a message you need to `accept`.
2. To configurate the paths, you need to navigate in PHPStorm to `Settings` and search there for `Server`. You will find `PHP -> Servers`. There you select `localhost` and exclude `Project Files`.
4. Behind the `shopware` folder under `Absolute Path Mapping` you need to enter `/var/www/html`. 
4. Behind the `public` inside the `shopware` folder under `Absolute Path Mapping` you need to enter `/var/www/html/public`. 
5. Click `Apply` and `OK` to save the settings.
6. Now you can create breakpoints inside the code and debug.

See official Dockware Documentation here: [Dockware](https://docs.dockware.io/features/switch-php-version)


<hr>

## 8. PHP Version configuration & switch
You can switch via PHP Versions while runtime. Default PHP Version is 7.4.
When you are in `/html` path, you can change PHP version via:
<br>
`make switch-php version=8.2` for PHP8.2
<br>
`make switch-php version=8.1` for PHP8.1
<br>
`make switch-php version=8.0` for PHP8.0
<br>
`make switch-php version=7.4` for PHP7.4

See official Dockware Documentation here: [Dockware](https://docs.dockware.io/features/switch-php-version)

<hr>

## 9. Node Version configuration & switch
You can switch via Node Versions while runtime. Default Node version is 12.
When you are in `/html` path, you can change PHP version via:
<br>
`nvm use 16` for Node 16
<br>
`nvm use 14` for Node 14
<br>
`nvm use 12` for Node 12

See official Dockware Documentation here: [Dockware](https://docs.dockware.io/features/switch-node-versions)

<hr>

## 10. Remote host & local database configuration in PHPStorm
### 10.1 Create Docker Container Connection via SFTP
1. Navigate in PHPStorm to `Tools -> Deployment -> Configuration`
2. Inside the open window click `+`, name it like `localhost` and create.
3. At `SSH Configuration` click on the `...` and create a new connection via `+` in the next opening window.
4. Here you need to fill some Variables:
>- Host: localhost
>- Port: 22
>- Username: dockware
>- Password: dockware
>- Check save password
>- Check Parse config file
>
5. Now you can click on `Test connection`. The test should be give you a successfully answer. In case of a successfully answer, you can save the configuration.
6. Select now the created SSH Connection. At Point 3 of this documentation, you will not click `...`, but choose the ssh connection you just created.
7. At `Root Path` you need to fill it with `/`. Test the connection now and in case of success, save it.
8. For the last step, Navigate to `Tools -> Deployment` and click `Browse Remote Host`.

### 10.2. Create Docker Database Connection

<hr>

## 11. Working with watchers
To use the watchers in the `docker-compose.yml` file at `services -> shopware -> ports` the following ports must be enabled:
> 8888, 9998, 9999
>
In our standard this is automatically enabled.<br>
If you want to enable the watcher for storefront or administration, you can follow the next steps:
1. Connect to you docker container inside the terminal.
2. For start storefront watcher: `cd ~/var/www && make watch-storefront`
3. For stop storefront watcher: `cd ~/var/www && make stop-watch-storefront`
4. For start administration watcher: `cd ~/var/www && make watch-admin`
5. For stop administration watcher: `cd ~/var/www && make stop-watch-admin`
> Notice: We have created a makefile with some easy alias to start/stop the watchers.
>
<br>
After you have start the watcher of your choice, the shop ist now available under:
- Storefront: `http://localhost:9998`
- Administration: `http://localhost:8888`

<hr>

## 12 Working with makefile
The makefile ist located inside the project folder and the mapped shopware folder of the project.<br>
Inside the makefile are much useful commands, which can used inside & outside from the docker-container.<br>
In the project folder makefile, only docker start/exec/stop commands are located. Inside the shopware folder makefile, there are much helpful commands.
<br>
<br>
For example, to start a docker container u can use in project folder (not shopware folder) `make docker-start`. After docker container is started, use `make docker-exec` to get the cli from docker container.<br>
With the tabular u will get an autocompletion.
<br>
Inside the docker-container cli, you get the mapped makefile.<br>
For example `make theme` executes the following commands: `bin/console theme:compile && bin/console cache:clear`. You can save yourself a lot of work that way.

### All makefile useful commands:

- `make docker-start` = start docker container (only usable in shopware project root path - docker-compose.yml needed)
- `make docker-stop` = stops docker container (configuration per project needed - name variables)
- `make docker-exec` = enters docker container cli (configuration per project needed - name variables)
- `make cache` = bin/console cache:clear 
- `make theme` = bin/console theme:compile && bin/console cache:clear 
- `make plugin-refresh` = bin/console theme:refresh
- `make build-js` = bin/build-js.sh
- `make build-storefront` = bin/build-storefront.sh
- `make build-admin` = bin/build-administration.sh
- `make watch-store` = starts watcher for storefront at http://localhost
- `make watch-admin` = starts watcher for Shopware version higher than 6.4.14.0 Admin at http://localhost:8888
- `make stop watcher` = stops all watchers
<br>
<br>
<hr>

## 13. Troubleshooting
### Allowed memory size exhausted
Sometimes when you use `bin/console` or `composer` commands, you can get an `memory_limit` error.<br>
In case of a memory_limit error, try to put an `memory_limit=-1`at the start of the command.<br>
This should show like this: `memory_limit=-1 bin/console plugin:refresh`.
<br>
<br>
<hr>

## 14 Standard Plugins & Wildcard instance configuration
### Standard Plugins
1. Frosh Tools
2. Frosh Adminer
3. Frosh Development Helper

### Wildcard instance configuration
auth token generate & add standard plugins license


## 15 Error's
### T_DOUBLE_ARROW
- This is a function of php version 8.0 or higher. With PHP 7.4 you get this error.
- So you should increase the php version to 8.0 or higher.
- On local environment do that like [here](#8-php-version-configuration--switch)
- On stage & live environment, you need to check the server-configuration site.