#!/usr/bin/env bash

# Deployment script - run this script with root privileges

HOME_DIR=${1:-/var/www/share/hrs-prototype6.c-2348.maxcluster.net}
MODE=${2:-mode}

if [[ "$MODE" != production ]] && [[ "$MODE" != staging ]] && [[ "$MODE" != dev1 ]]; then
  echo "### MODE must be either 'staging', 'dev1' or 'production'"
  exit 1
fi

echo "#"
echo "##"
echo "### Running deploy script in mode: $MODE"
echo "##"
echo "#"

PHP_CMD='php'

FORMER=$(readlink -f "$HOME_DIR/htdocs")
SHARED=$HOME_DIR/shared
ACTIVE=$HOME_DIR/$(date +"%y%m%d%H%M%S")

echo "### Former: "$FORMER
echo "### Shared: "$SHARED
echo "### Active: "$ACTIVE

mv "$HOME_DIR/_tmp" "$ACTIVE"

# avoid non existing directory errors later
mkdir -p "$ACTIVE/var"
mkdir -p "$ACTIVE/var/cache"
#
mkdir -p "$HOME_DIR/shared/files"
mkdir -p "$HOME_DIR/shared/var/log"

# set symlinks to sources not in deployment
rm -rf "$ACTIVE/var/log" && ln -s "$HOME_DIR/shared/var/log" "$ACTIVE/var/log"
rm -rf "$ACTIVE/config/jwt" && ln -s "$HOME_DIR/shared/config/jwt" "$ACTIVE/config/jwt"
rm -rf "$ACTIVE/.env" && ln -s "$HOME_DIR/.env" "$ACTIVE/.env"
rm -rf "$ACTIVE/public/adminer_ic.php" && ln -s "$HOME_DIR/adminer_ic.php" "$ACTIVE/public/adminer_ic.php"

rm -rf "$ACTIVE/files" && ln -s "$HOME_DIR/shared/files" "$ACTIVE/files"
rm -rf "$ACTIVE/public/media" && ln -s "$HOME_DIR/shared/public/media" "$ACTIVE/public/media"
rm -rf "$ACTIVE/public/sitemap" && ln -s "$HOME_DIR/shared/public/sitemap" "$ACTIVE/public/sitemap"
rm -rf "$ACTIVE/public/theme" && ln -s "$HOME_DIR/shared/public/theme" "$ACTIVE/public/theme"
rm -rf "$ACTIVE/public/thumbnail" && ln -s "$HOME_DIR/shared/public/thumbnail" "$ACTIVE/public/thumbnail"
rm -rf "$ACTIVE/solfusion" && ln -s "$HOME_DIR/shared/solfusion" "$ACTIVE/solfusion"

if [[ "$MODE" = staging ]]; then
  rm -rf "$ACTIVE/public/_dev1" && ln -s "$HOME_DIR/../dev1/hrs-prototype6.c-2348.maxcluster.net/public" "$ACTIVE/public/_dev1"
fi
if [[ "$MODE" = production ]]; then
  rm -rf "$ACTIVE/public/_staging" && ln -s "$HOME_DIR/htdocs/public" "$ACTIVE/public/_staging"
fi

echo "### Block access for both backend/frontend from now on"
mkdir -p "$FORMER/update-assets"

echo '### Update plugins'
cd "$ACTIVE" &&bin/console cache:clear -n &&
 bin/console plugin:refresh | grep -E '.*Yes[[:space:]]+$' | awk '{if ($1) system("sudo -u www-data bin/console plugin:update -r -c -n " $1) }' &&
 bin/console cache:clear -n

echo "### Run migrations"
cd "$ACTIVE" && bin/console database:migrate --all -n

echo "### Compile theme"
cd "$ACTIVE" && bin/console theme:compile -n

echo "### Install assets"
cd "$ACTIVE" && bin/console assets:install -n

echo "### Delete shopware instances"
chmod +x $ACTIVE/_deploy/common/clean_shopware_instances.sh
/bin/bash "$ACTIVE/_deploy/common/clean_shopware_instances.sh"

echo "### Clean up unused subfolders"
rm -rf "$ACTIVE/_deploy"

# swap directories
echo "### Hot swapping sources"
rm -rf "$HOME_DIR/htdocs" && ln -sf "$ACTIVE"/public "$HOME_DIR/htdocs"
rm -rf "$HOME_DIR/shopware" && ln -sf "$ACTIVE" "$HOME_DIR/shopware"
