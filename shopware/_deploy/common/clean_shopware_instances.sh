#!/bin/bash

STAGING_PATH="$HOME"/hrs-prototype6.c-2348.maxcluster.net/
echo "++++++++++++++++++++++++STAGING_PATH++++++++++++++++++++++++";
echo $STAGING_PATH;
echo "++++++++++++++++++++++++STAGING_PATH++++++++++++++++++++++++";
search_dir_array=()
counter=0
search_dir=`ls $STAGING_PATH | grep ^[0-9]`
for directory in $search_dir
    do
        search_dir_array+=($directory)
        ((counter=counter+1))
done

if (($counter>1))
then
    for ((i=0;i<counter-1;i++))
    do
        echo "Deleting directory "$STAGING_PATH"${search_dir_array[$i]}"/
        rm -rf $STAGING_PATH"${search_dir_array[$i]}"/
    done
fi
