# Shopware 6 intoCommerce standard giude
## Customize Dockware docker-compose.yml file
1. change image to your selected shopware version, do not use latest. When you chose latest, there are automatically shopware updates and you wil not get notified about that.
    - shopware:
        - image: dockware/dev:6.4.18.1
2. change container-name name to a project specific name
    - shopware:
        - container_name: projectxy_container


## Create Dockware Project (initial setup)
1. edit local docker-compose.yml file.
2. start you docker-container with `docker-compose up -d`
3. create a directory, where your shop files should be located `mkdir -p ./{{foldername}}`
4. copy the shop files into your local created shop files folder `docker cp {{container-name}}:/var/www/html/. ./{{foldername}}`
5. that the changes on you local machine are connecting to the doker-container, you have to execute following statement on your local terminal: `docker exec -it {{container-name}} bash -c 'sudo chown www-data:www-data /var/www/html -R'`
6. to enter the docker-container terminal use `docker exec -it {{container-name}} bash`

## Start a Dockware project
1. to start the project navigate in the terminal to docker-compose.yml and execute: `docker-compose up -d`
2. to enter the docker-container terminal use `docker exec -it {{container-name}} bash`

## Initial intoCommerce Standard Project Setup
1. to start the project navigate in the terminal to docker-compose.yml and execute: `docker-compose up -d`
2. to enter the docker-container terminal use `docker exec -it {{container-name}} bash`
3. Run `composer install`
4. Open `http://localhost/installer` and run through the shopware installer
5. Open `http://localhost/admin` and finish the installer
6. Run `bin/console theme:compile && bin/console cache:clear`
